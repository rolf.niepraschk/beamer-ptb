\listfiles
\documentclass[aspectratio=169]{ptbbeamer}

%---------------------------------------------------
\newcommand\DeEn[2]{%
  \begingroup
  \selectlanguage{ngerman}#1\,/\,\selectlanguage{english}\textit{#2}%
  \endgroup
}
\newcommand*\cmd[1]{\texttt{\textbackslash #1}}
%---------------------------------------------------

\newcommand*\thisShortTitle{ptbbeamer -- Dokumentation / documentation}

\title[\thisShortTitle]%
  {\LaTeX-Klasse\,/\,\textit{\LaTeX\ class} \\ ptbbeamer}
\subtitle{Dokumentation\,/\,\textit{documentation}}
\date{2022/08/08}
\author{Rolf~Niepraschk}

\AddToHook{begindocument/before}{%
  \hypersetup{pdftitle={\thisShortTitle}}
}% instead of the very long title

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}[t]
\frametitle{\DeEn{\translate{Introduction}}{\translate{Introduction}}}
\begin{columns}[T]% 
  \begin{column}{.5\dimexpr\textwidth-\columnsep}
    \setlength\rightskip{0pt}% allow hyphenation
    \selectlanguage{ngerman}
    Die Dokumentenklasse »ptbbeamer« basiert auf der Dokumentenklasse
    »beamer«, an die sämtliche Klassenparameter weitergeleitet werden (zu
    Details siehe:~\cite{BEAMER}). Dieses Layout entspricht der PTB-Vorgabe
    (Powerpoint). Standardmäßig ist die deutsche Spracheinstellung mit neuer
    Rechtschreibung aktiviert (\texttt{ngerman}). Die englische
    Spracheinstellung wird zusätzlich geladen und kann bei Bedarf aktiviert
    werden. Standard ist das Seitenverhältnis 4:3. Die Klassenoption
    \texttt{169} wählt das Seitenverhältnis 16:9.\\[1ex] Die \LaTeX"=Version
    darf nicht älter als von Oktober 2020 sein und zum Kompilieren muss
    \texttt{lualatex} oder \texttt{xelatex} verwendet werden.
  \end{column}%
  \hfill
  \begin{column}{.5\dimexpr\textwidth-\columnsep}
    \setlength\rightskip{0pt}% allow hyphenation \selectlanguage{english}
    \itshape The document class ``ptbbeamer'' is based on the document class
    ``beamer'', to which all class parameters are forwarded (for details
    see:~\cite{BEAMER}). This layout corresponds to the PTB specification
    (Powerpoint). By default, the language setting is German with new
    spelling activated (\texttt{ngerman}). The English language setting will
    be additionally loaded and can be activated if required. The aspect ratio
    is 4:3 by default. The class option \texttt{169} selects the aspect ratio
    16:9.\\[1ex] The \LaTeX\ version must not be older than October 2020 and
    \texttt{lualatex} or \texttt{xelatex} must be used for compiling.
  \end{column}%
\end{columns}
\end{frame}

\begin{frame}[t,allowframebreaks,fragile]
\frametitle{\DeEn{Anwendung}{Usage}}

\setbeamertemplate{blocks}[rounded][shadow=false]

\begin{columns}[T]%
  \begin{column}{.5\dimexpr\textwidth-\columnsep}
    \selectlanguage{ngerman}
    \begin{block}{Die Bedeutung der Makros}  
    \begin{itemize}
      \item\cmd{orgLogo}: Fügt das Logo der betreffenden Organisationseinheit 
        rechtsbündig im Kopf der Titelseite ein. Als Parameter wird der Dateiname 
        einer Grafikdatei erwartet (erlaubte Dateitypen: {\ttfamily .pdf, .mps, 
        .png, .jpg}).
      \item\cmd{finalpage}: Fügt innerhalb einer frame-Umgebung eine spezielle 
        Schlussseite ein. Konfiguriert wird sie mit Angaben zu \texttt{name}, 
        \texttt{email}, \texttt{phone} und \texttt{location}.
    \end{itemize}
    \end{block} 
  \end{column}%
  \hfill
  \begin{column}{.5\dimexpr\textwidth-\columnsep}
    \selectlanguage{english} \itshape 
    \begin{block}{\itshape The meaning of the macros} 
    \begin{itemize}
      \item\cmd{orgLogo}: Inserts the logo of the relevant organizational unit 
        right-aligned in the header of the title page. The file name of a 
        graphics file is expected as a parameter (permitted file types: 
        {\ttfamily .pdf, .mps, .png, .jpg}).
      \item\cmd{finalpage}: Inserts a special final page within a frame 
        environment. It is configured with information about \texttt{name},
        \texttt{email}, \texttt{phone} and \texttt{location}.
    \end{itemize}
    \end{block} 
  \end{column}%
\end{columns}

\framebreak

\begin{columns}[T]%
  \begin{column}{.5\dimexpr\textwidth-\columnsep}
    \selectlanguage{ngerman}
    \begin{itemize}
      \setlength\rightskip{0pt}% allow hyphenation
      \item\cmd{normalHeader}, \cmd{reverseHeader}: Reihenfolge von 
        »frame title« und PTB-Logo 
      \item\cmd{normalFooter}, \cmd{reverseFooter}:
        Reihenfolge von »subtitle«, Seitenzahl und Datum
      \item\cmd{showProgressbar}:
        Anzeigen eines Fortschrittsbalkens in der Fußzeile
      \item\cmd{vUnit}: Längenregister, dessen Wert \cmd{paperheight}$/1000$
      beträgt; geeignet für einheitliche vertikale Abstände und
      Positionierungen (\texttt{4:3 und 16:9}).
    \end{itemize}
  \end{column}%
  \hfill
  \begin{column}{.5\dimexpr\textwidth-\columnsep}
  \selectlanguage{english} \itshape 
    \begin{itemize}
      \setlength\rightskip{0pt}% allow hyphenation
      \item\cmd{normalHeader}, \cmd{reverseHeader}:
        Order of »frame title« und PTB Logo
      \item\cmd{normalFooter}, \cmd{reverseFooter}: 
        Order of »subtitle«, page number and date
      \item\cmd{showProgressbar}:
        Show a progress bar in the footer
      \item\cmd{vUnit}: Length register whose value is
      \cmd{paperheight}$/1000$ amounts to; suitable for uniform vertical
        spacing and positioning (\texttt{4:3 and 16:9}).
    \end{itemize}
  \end{column}%
\end{columns}

\framebreak

\begin{columns}[T]% 
  \begin{column}{.5\dimexpr\textwidth-\columnsep}
    \selectlanguage{ngerman}
    \begin{block}{Unterstützung von »siunitx« (\translate{examples})}
      \begin{itemize} 
        \item \verb!\num{3.1415926535}! \\ $ \Longrightarrow \num{3.1415926535}$ 
        \item \verb!\SI{7.5E-5}! \verb!{\pascal\cubic\metre\per\second}!
          $\Longrightarrow \SI{7.5E-5}{\pascal\cubic\metre\per\second}$ 
        \item \verb!\SI{295.939+-0.049}{\kelvin}! \\ $\Longrightarrow 
          \SI{295.939+-0.049}{\kelvin}$ 
        \item \verb!\qtyrange{10}{30}{\metre}! \\ $\Longrightarrow 
          \qtyrange{10}{30}{\metre}$
        \item Für weitere Informationen siehe~\cite{SIUNITX}
      \end{itemize} 
    \end{block}
  \end{column}%
  \hfill
  \begin{column}{.5\dimexpr\textwidth-\columnsep}
    \selectlanguage{english} 
    \begin{block}{\itshape Support of »siunitx« (\translate{examples})}
      \begin{itemize} 
        \item \verb!\num{3.1415926535}! \\ $ \Longrightarrow \num{3.1415926535}$ 
        \item \verb!\SI{7.5E-5}! \verb!{\pascal\cubic\metre\per\second}!
          $\Longrightarrow \SI{7.5E-5}{\pascal\cubic\metre\per\second}$ 
        \item \verb!\SI{295.939+-0.049}{\kelvin}! \\ $\Longrightarrow 
          \SI{295.939+-0.049}{\kelvin}$
        \item \verb!\qtyrange{10}{30}{\metre}! \\ $\Longrightarrow 
          \qtyrange{10}{30}{\metre}$
        \item For more information see~\cite{SIUNITX}
      \end{itemize} 
    \end{block}
  \end{column}%
\end{columns}

\end{frame}

\begin{frame}[t]
  \frametitle{\DeEn{\bibname}{\bibname}}
  \vspace{1ex}
  \begin{thebibliography}{99}
    \bibitem{BEAMER}[1]
    Till Tantau, Joseph Wright, Vedran Miletić:
    \newblock The beamer class -- User Guide for version 3.67; May 17, 2022;
    \newblock
    \href{https://mirrors.ctan.org/macros/latex/contrib/beamer/doc/beameruserguide.pdf}%
    {CTAN:~beameruserguide.pdf}.
    \bibitem{SIUNITX}[2]
    Joseph Wright
    \newblock 
    siunitx -- A comprehensive (si) units package; June 22, 2022;
    \newblock
    \href{https://mirrors.ctan.org/macros/latex/contrib/siunitx/siunitx.pdf}%
    {CTAN:~siunitx.pdf}.
  \end{thebibliography}
\end{frame}

\begin{frame}
  \finalpage[name=Rolf Niepraschk,email=Rolf.Niepraschk@ptb.de,%
    phone=(030) 3481-7316,location=BLN]%
  \centering\huge
  Vielen Dank für Ihre Aufmerksamkeit! \par
  \selectlanguage{english}
  \textit{Thank you for your attention!}
  \vspace{12ex}
\end{frame}

\end{document}


