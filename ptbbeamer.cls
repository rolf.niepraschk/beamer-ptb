
\setcounter{errorcontextlines}{100}
\listfiles
\def\PTB@classname{ptbbeamer}

\ProvidesClass{\PTB@classname}[2022/08/07 v2.1.1 PTB beamer class (RN)]

\newcommand*\PTB@tempa{}
\newcommand*\PTB@tempb{}
\newcommand*\PTB@tempc{}

% The default language should be `ngerman', `english' should also be present.
\def\PTB@tempa{english}\def\PTB@tempb{english,ngerman}
\@for\CurrentOption:=\@classoptionslist\do{%
  \ifx\CurrentOption\PTB@tempa
    \def\PTB@tempb{ngerman}% Only add `english' if not already present.
  \fi
}%
\edef\@classoptionslist{\PTB@tempb,\@classoptionslist}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{beamer}}
\ProcessOptions\relax

\LoadClass[onlytextwidth]{beamer}

\RequirePackage{iftex,babel,bookmark}
\RequirePackage[autostyle=true,german=guillemets,maxlevel=3]{csquotes}% 
\MakeAutoQuote{»}{«}

\usetheme{PTB}

\RequirePackage{siunitx}% 
\addto\extrasngerman{\sisetup{locale=DE}}
\addto\extrasenglish{\sisetup{locale=US}}
\sisetup{%
,bracket-ambiguous-numbers = true
,per-mode = power
,tight-spacing = false
,uncertainty-mode = separate
,reset-text-series = false, text-series-to-math = true% vormals: detect-weight=true
,reset-text-family = false, text-family-to-math = true% vormals: detect-family=true
,list-units = bracket
}
 
\endinput
